
$('#list-open-leads tr').click(function () {
    var url = "/leads/detail/" + $(this).data('id');
    $.ajax({
        url: url,
        type: "GET",
        async: false,
        success: function (data) {
            console.log(data);
            if (!$(document).is('#error'))  $("#error").remove();
            if (!$('table').is('#detail-one-lead')) {
                $("#block-all-lead").removeClass("col-lg-10");
                $("#block-all-lead").addClass("col-lg-7");
                $("#wrap .row").append('' +
                    '<div class="col-lg-5 col-xs-12">' +
                    '<div id="block-one-lead"></div>' +
                    '</div>');
                createTableForDetailLead(data[0]);
            }
            else {
                for(var value in data[0])
                    $('#' + value + '-one-detail').text(data[0][value]);
            }
        },
        error: function () {
            $("#detail-one-lead").remove();
            $("#block-one-lead").append('<h3 id="error">Sorry, information was not found or an error occurred, please contact the administrator</h3>')
        }
    });
});

function createTableForDetailLead(data) {
    $("#block-one-lead").append(
        '<table class="table" id="detail-one-lead">' +
        '<thead>' +
        '<tr>' +
        '<th scope="col">Key</th>' +
        '<th scope="col">Value</th>' +
        '</tr>                ' +
        '</thead>' +
        '<tbody>' +
        '<tr>' +
        '<th>icon</th>' +
        '<td id="icon-one-detail"></td>' +
        '</tr>' +
        '<tr>' +
        '<th>date</th>' +
        '<td id="date-one-detail">' + data.date + '</td>' +
        '</tr>' +
        '<tr>' +
        '<th>name</th>' +
        '<td id="name-one-detail">' + data.name + '</td>' +
        '</tr>' +
        '<tr>' +
        '<th>phone</th>' +
        '<td id="phone-one-detail">Radio</td>' +
        '</tr>' +
        '<tr>' +
        '<th>email</th>' +
        '<td id="email-one-detail">' + data.email + '</td>' +
        '</tr>' +
        '<tr>' +
        '<th>Radio</th>' +
        '<td id="radio-one-detail">' + data.radio + '</td>' +
        '</tr>' +
        '<tr>' +
        '<th>Checkbox</th>' +
        '<td id="checkbox-one-detail">' + data.checkbox + '</td>' +
        '</tr>' +
        '</tbody>' +
        '</table>'
    )
}