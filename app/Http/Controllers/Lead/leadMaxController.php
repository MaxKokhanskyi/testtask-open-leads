<?php

namespace App\Http\Controllers\Lead;

use App\Models\Lead;
use App\Http\Controllers\Controller;

/*
// START Test task Maxim 02/2017
*/
class leadMaxController extends Controller
{
    public function index(){
        $lead = new Lead();
        $openLeads = $lead->getOpenLeads();
        return view('lead.app', ['openLeads' => $openLeads]);


    }
    public function getDetailOpenLead($idLead){
        $lead = new Lead();
        $openLead = $lead->getDetailOneOpenLead($idLead);
        return response()->json($openLead);
    }
}
/*
 * END test task Maxim 02/2017
 */

