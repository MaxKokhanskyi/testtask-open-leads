<?php

namespace App\Models;

use Cartalyst\Sentinel\Users\EloquentUser;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\DB;


#class Lead extends EloquentUser implements AuthenticatableContract, CanResetPasswordContract {
#    use Authenticatable, CanResetPassword;
class Lead extends EloquentUser {

    protected $table="leads";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'agent_id','sphere_id','name', 'customer_id', 'comment', 'date', 'bad'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    #protected $hidden = [
    #    'password', 'remember_token',
    #];

    public function sphere(){
        return $this->hasOne('App\Models\Sphere', 'id', 'sphere_id');
    }

    public function info(){
        return $this->hasMany('App\Models\LeadInfoEAV','lead_id','id');
    }

    public function phone(){
        return $this->hasOne('App\Models\Customer','id','customer_id');
    }

    public function obtainedBy($agent_id = NULL){
        $relation = $this->belongsToMany('App\Models\Agent','open_leads','lead_id','agent_id');
        return ($agent_id)? $relation->where('agent_id','=',$agent_id) : $relation;
    }
    /*
    //Start test task Maxim 02/2017
    */
    public function getOpenLeads(){
        return $openLeads = DB::table('leads')
            ->join('customers', 'customers.id', '=', 'leads.customer_id')
            ->join('open_leads', 'open_leads.lead_id', '=', 'leads.id')
            ->select('leads.id','leads.date', 'leads.name', 'leads.email', 'customers.phone')
            ->get();
    }
    public function getDetailOneOpenLead($idOpenLead = NULL){
        if($idOpenLead == NULL) return false;
        $openLead = DB::table('leads')
            ->join('customers', 'customers.id', '=', 'leads.customer_id')
            ->join('open_leads', 'open_leads.lead_id', '=', 'leads.id')
            ->select('leads.date', 'leads.name', 'leads.email', 'customers.phone')
            ->where('leads.id', '=', $idOpenLead)
            ->get();
        $attrs = $this->getArAttrDetailOneOpenLead($idOpenLead);
        $openLead[0]->radio = $attrs->where('label', 'Radio')->implode('value', ', ');;
        $openLead[0]->checkbox = $attrs->where('label', 'CheckBox')->implode('value', ', ');
        return $openLead;
    }
    public function getArAttrDetailOneOpenLead($idOpenLead = NULL){
        if($idOpenLead == NULL) return false;
        $allAttrOpenLead = DB::table('leads')
            ->join('sphere_attributes', 'leads.sphere_id', '=', 'sphere_attributes.sphere_id')
            ->join('sphere_attribute_options', 'sphere_attributes.id', '=', 'sphere_attribute_options.sphere_attr_id')
            ->select('leads.sphere_id AS XX', 'sphere_attributes.id AS AID',
                'sphere_attribute_options.id AS OID', 'sphere_attribute_options.id as SAP')
            ->where('leads.id', '=', $idOpenLead)
            ->get();
        $activeAttributes = collect([]);
        foreach ($allAttrOpenLead as $attrOpenLead){
            $activeOneAttrOpenLead = DB::table('leads')
                ->join('sphere_attributes', 'leads.sphere_id', '=', 'sphere_attributes.sphere_id')
                ->join('sphere_attribute_options', 'sphere_attributes.id', '=', 'sphere_attribute_options.sphere_attr_id')
                ->join('sphere_bitmask_' . $attrOpenLead->XX, 'sphere_bitmask_' . $attrOpenLead->XX .'.user_id', '=', 'leads.id')
                ->select('sphere_attributes.label AS label', 'sphere_attribute_options.value AS value')
                ->where('leads.id', '=', $idOpenLead)
                ->where('fb_' . $attrOpenLead->AID . '_' . $attrOpenLead->OID, '=', 1)
                ->where('sphere_attribute_options.id', '=', $attrOpenLead->SAP)
                ->where('sphere_bitmask_' . $attrOpenLead->XX . '.type', '=', 'lead')
                ->get();
            if(count($activeOneAttrOpenLead)>0) $activeAttributes->push($activeOneAttrOpenLead[0]);
        }
        return $activeAttributes;
    }
/*
 * END test task Maxim 02/2017
 */
}
