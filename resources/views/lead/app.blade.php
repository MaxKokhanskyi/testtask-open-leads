<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OpenLeads</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ asset('components/bootstrap/css/bootstrap.min.css') }}">
</head>
<body>
<div class="container" id="wrap">
    <h2 class="text-center">OpenLeads</h2>
    <div class="row">
        <div id="block-all-lead" class="col-lg-10 col-xs-12">
            <table class="table" id="list-open-leads">
                <thead>
                <tr>
                    <th scope="col">icon</th>
                    <th scope="col">date</th>
                    <th scope="col">name</th>
                    <th scope="col">phone</th>
                    <th scope="col">email</th>
                </tr>
                </thead>
                <tbody>
                @foreach($openLeads as $openLead)
                    <tr data-id="{{$openLead->id}}">
                        <td></td>
                        <td>{{$openLead->date}}</td>
                        <td>{{$openLead->name}}</td>
                        <td>{{$openLead->phone}}</td>
                        <td>{{$openLead->email}}</td>
                        <td>
                            <button type="button" class="btn btn-default btn-sm">
                                <span class="glyphicon glyphicon-chevron-right"></span> Detail
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-lg-5 col-xs-12">
            <div id="block-one-lead"></div>
        </div>
    </div>

</div>
<!-- jQuery -->
<script type="text/javascript" src="{{ asset('components/jquery/jquery-2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/web/js/open-lead.js') }}"></script>

</body>
</html>